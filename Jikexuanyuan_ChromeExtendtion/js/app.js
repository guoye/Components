window.onload = function(){
  new Vue({
    el: '#app',
    data: {
      copyright: decodeURI('%E5%BC%A0%E5%9B%BD%E4%B8%9A%E5%8F%8B%E6%83%85%E5%A5%89%E7%8C%AE'),
      startState: str2Boolean(localStorage.startState||''),
      time: Number(localStorage.time||5),
      startPrice: Number(localStorage.startPrice||5),
      endPrice: Number(localStorage.endPrice||25),
      hideWechat: true,
      autoGrab: str2Boolean(localStorage.startState||false)
    },
    methods: {
      getStatus: function(startState){
        startState ? this.startState = false : this.startState = true;
        localStorage.startState = this.startState;
        localStorage.time = this.time;
        localStorage.startPrice = this.startPrice;
        localStorage.endPrice = this.endPrice;
        localStorage.autoGrab = this.autoGrab;

        this.listener();
      },
      listener: function(){
        checkIsJike(this.$data);
      }
    }
  })
}

function str2Boolean(str){
  if(str == 'true') return true;
  return false;
}

function backgroundJSAction(opts, tab){
  var backgroundJS = chrome.extension.getBackgroundPage();

  if(opts.startState){
    backgroundJS.timer.start(opts, tab);
  }else{
    backgroundJS.timer.stop(opts, tab);
  }
}

// 监听极客网站
function checkIsJike(data){

  chrome.tabs.query({'url': 'http://fuwu.jikexueyuan.com/homework/not-corrected'}, function(tabs) {
    if( tabs.length == 0 ){
      chrome.tabs.create({
        index: 0,
        url: 'http://fuwu.jikexueyuan.com/homework/not-corrected',
        active: true
      },function(tab){
        console.log(2);
        backgroundJSAction(data, tab);
      });
    }else{
        console.log(3);
      backgroundJSAction(data, tabs[0]);
    }
  });
}
