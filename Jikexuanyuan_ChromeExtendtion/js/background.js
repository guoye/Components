// Copyright (c) 2016 The Chromium Authors. All rights reserved.
// Authors: Guoye

// chrome.browserAction.onClicked.addListener(function(tab) {
//   // chrome.tabs.executeScript(null, {file: "js/content_script.js"});
// })


chrome.runtime.onConnect.addListener(function(port) {
  console.assert(port.name == "setCount");
  port.onMessage.addListener(function(msg) {
    window.timer.setCount(msg.count)
  });
});

(function() {
    var timer = null;
    window.timer = {
        start: function(opts, tab) {
          console.log(opts);
          console.log(tab);
          var _this = this;
          timer = setInterval(function() {
            // setTimeout(function(){})
            // chrome.tabs.reload(tab.id, null, function(){})
            _this.sendToContent(opts, tab);
          }, Number(opts.time)*1000);
        },
        stop: function(opts) {
            clearInterval(timer);
            chrome.browserAction.setBadgeText({
                text: ""
            });
        },
        //设置插件数字
        setCount: function(count){
          chrome.browserAction.setBadgeText({
              text: count + ""
          });
        },
        // 向指定tab的content_script发送消息，执行方法
        sendToContent: function(data, tab){
          var _this = this;
          var msgObj = data;
          msgObj.action = 'start';
          chrome.tabs.sendMessage(tab.id, msgObj, function(response){
            console.log(response);
            if(response && response.count > 0){
              _this.stop();
            }
          })
        }
    }
})();










// //取消请求
// var blockingXHR = false; //默认不取消
// //请求过滤
// var requestFilters = {
//   urls: [
//     // "<all_urls>"
//     "http://localhost:8080/topui/data/*", "http://localhost:18080/test/*"
//     // "*://*/*"
//   ]
// };
// chrome.webRequest.onBeforeRequest.addListener(
//   function(info) {
//     var url = 'http://localhost:18080/test/org',
//         type = info.method, data;
//
//     if (type == 'POST') {
//       data = info.requestBody.formData;
//       data.url = info.url;
//     }else if(type == 'GET'){
//       var surl = info.url;
//       if(surl.indexOf('?') > 0){
//         data = surl.substring(surl.indexOf('?')+1);
//         data += '&url='+surl.substring(0, surl.indexOf('?'));
//       }
//     }else{
//       data = '';
//     }
//     $.ajax({
//       url: url,
//       type: type,
//       data: data,
//       dataType: 'json',
//       async: false,
//       success: function(res){
//         if(res.validate != 'success'){
//           chrome.tabs.executeScript({
//             // code: 'document.body.style.backgroundColor="red"'
//             code: 'alert("校验失败")'
//           });
//           blockingXHR = true;
//         }else{
//           blockingXHR = false;
//         }
//       },
//       error: function(err){
//         console.log(err)
//       }
//     })
//
//     return {
//       // redirectUrl: 'http://localhost:18080/test/org'
//       // redirectUrl: 'http://localhost:8080/topui/data/selectcustom32.json'
//       cancel: blockingXHR
//     };
//   },
//   // filters
//   requestFilters,
//   // extraInfoSpec
//   ["blocking","requestBody"]);
